## Synopsis
A collection of scripts I threw together to mess with the Slack API.

## Requirements
Relies on [slacker](https://github.com/os/slacker) for the python scripts


[slack-ruby-client](https://github.com/slack-ruby/slack-ruby-client) for the ruby script

`gem install slack-ruby-client`

Latest version of the ruby [faker](https://github.com/stympy/faker) gem


* Clone the Git repository.

`$ git clone https://github.com/stympy/faker.git`

* Change to the new directory.

`cd faker`

* Build the gem.

`$ rake build gem`

* Install the gem.

`$ gem install pkg/gemname-1.23.gem`


For the python packages

`pip install slacker`

`pip install names`

Legacy web token from [here](https://api.slack.com/custom-integrations/legacy-tokens)

## Motivation
Instead of doing something productive with my Thursday night, I felt like messing with the slack API.


My friends and I were thinking of silly names for a channel, and then this repo happened.

I'm sorry

## Use

`rename_channel_to_something_lewd.py`

Generates a vulgar name and sends an api request to rename your specified channel

`slack_change_profile.py`

Forked from [here](https://github.com/gabinante/TitleCreator)

Generates a random name, job title, and sets your profile picture to a random shiba inu.

Can be extended to change more of the profile fields if you so desire

![alt text](https://i.imgur.com/gvwxmXu.png "shiba dev ops")


## API Reference
Get familiar with the [Slack web API](https://api.slack.com/web)
