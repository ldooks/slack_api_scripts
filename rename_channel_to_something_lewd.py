from slacker import Slacker
import random
import requests

channel_id = '<your channel id here>'
slack = Slacker('<your slack api token here>')

def get_a_name():
  # Pull the wordlist from some random github account
  request_wordlist = requests.get(
    'https://raw.githubusercontent.com/LDNOOBW/List-of-Dirty-Naughty-Obscene-and-Otherwise-Bad-Words/master/en'
  ).text.split('\n')

  # Replace all spaces with dashes
  words = [w.replace(' ', '-') for w in request_wordlist]

  # Join one to three random words with dashes (slack channel names cant have spaces)
  return '-'.join(random.sample(words, random.randint(1, 3)))

random_name = None
while not random_name:
  # Keep trying till we get a name shorter than
  # 22 characters (slacks channel name length limit)
  random_name = get_a_name()
  if len(random_name) <= 22:
    print "Selected %s" % random_name
    slack.channels.rename(channel_id, random_name)
  else:
    random_name = None
