require 'faker'
require 'json'
require 'open-uri'
require 'open3'
require 'slack-ruby-client'

token = 'your token here'

# Get a sheeb and save it to a local file
sheeb_url = open('http://shibe.online/api/shibes?count=1&urls=true').read.split('"')[1]
local_sheeb = '/tmp/sheeb.png'
File.open(local_sheeb, 'wb') do |fo|
  fo.write open(sheeb_url).read
end

puts Open3.capture2("/usr/bin/curl -s https://slack.com/api/users.setPhoto"\
  " -F \"token=#{token}\" -F \"image=@#{local_sheeb}\""
)
File.delete(local_sheeb)

my_profile = {}
my_profile['first_name'] = Faker::Name.first_name
my_profile['last_name'] = Faker::Name.last_name
my_profile['title'] = Faker::Name.unique.title
my_profile['status_emoji'] = Faker::SlackEmoji.emoji
my_profile['status_text'] = [
  Faker::Coffee.notes,
  Faker::HitchhikersGuideToTheGalaxy.quote,
  Faker::Matz.quote,
  Faker::RickAndMorty.quote,
  Faker::Robin.quote,
  Faker::TwinPeaks.quote,
].sample

Slack.configure do |config|
  config.token = token
end
client = Slack::Web::Client.new

puts client.users_profile_set(options={profile: my_profile.to_json})
